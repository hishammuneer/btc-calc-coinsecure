package com.hisham.btccalculator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.hisham.btccalculator.model.CoinsecureModel;
import com.hisham.btccalculator.model.CryptoCompareModel;
import com.hisham.btccalculator.model.ExchangeTypes;
import com.hisham.btccalculator.model.RequestModel;
import com.hisham.btccalculator.model.ZebPayModel;
import com.hisham.btccalculator.services.DownloaderService;

import org.apache.commons.math3.util.Precision;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.etBtc)
    EditText etBtc;
    @BindView(R.id.etAmount)
    EditText etAmount;
    @BindView(R.id.etSell)
    EditText etSell;
    @BindView(R.id.etBuy)
    EditText etBuy;

    @BindView(R.id.tvNewBtc)
    TextView tvNewBtc;
    @BindView(R.id.tvProfitLoss)
    TextView tvProfitLoss;
    @BindView(R.id.tvProfitLossRs)
    TextView tvProfitLossRs;

    @BindView(R.id.btnCalculate)
    Button btnCalculate;
    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.tvCsBuy)
    TextView tvCsBuy;
    @BindView(R.id.tvCsSell)
    TextView tvCsSell;

    @BindView(R.id.tvCryptoUSD)
    TextView tvCryptoUSD;
    @BindView(R.id.tvCryptoINR)
    TextView tvCryptoINR;
    @BindView(R.id.llSettings)
    ViewGroup llSettings;

    @BindView(R.id.etCsInrDiff)
    EditText etCsInrDiff;
    @BindView(R.id.etCryptoDiff)
    EditText etCryptoDiff;

    @BindView(R.id.tvZebPayBuy)
    TextView tvZebPayBuy;

    @BindView(R.id.tvZebPaySell)
    TextView tvZebPaySell;

    @BindView(R.id.swVibration)
    Switch swVibration;
    @BindView(R.id.swNotification)
    Switch swNotification;


    private AppSharedPrefs sharedPrefs;
    private double xBTC;
    private double xAmount;

    private BroadcastReceiver receiver;
    private double csBuy = 0;
    private double csSell = 0;
    private double zebPayModelBuy = 0;
    private double zebPayModelSell = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        sharedPrefs = AppSharedPrefs.getInstance(this);

        loadFromSpf();
        setReceiver();
        Intent intent = new Intent(this, DownloaderService.class);
        Bundle bundle = new Bundle();
        RequestModel requestModel = new RequestModel();
        bundle.putSerializable("RequestModel", requestModel);
//        bundle.putString("CallbackString", "progress_callback");
        startService(intent.putExtras(bundle)); // todo uncomment

        init();
        stuff();
        setSeekbar();
        llSettings.setVisibility(View.GONE);
    }

    private void init() {
        etCsInrDiff.setText(sharedPrefs.getINRDiff());
        etCryptoDiff.setText(sharedPrefs.getDollarDiff());
        swNotification.setChecked(sharedPrefs.getBooleanPref("Notification"));
        swVibration.setChecked(sharedPrefs.getBooleanPref("Vibration"));
    }

    private void stuff() {
        etCsInrDiff.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sharedPrefs.saveStringPreference("CsInrDiff", charSequence.toString());
            }
        });

        etCryptoDiff.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void afterTextChanged(Editable editable) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sharedPrefs.saveStringPreference("CryptoDiff", charSequence.toString());
            }
        });

        swNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedPrefs.saveBooleanPreference("Notification", b);
            }
        });

        swVibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedPrefs.saveBooleanPreference("Vibration", b);
            }
        });
    }

    @OnClick(R.id.tvCsBuy)
    public void loadBuy(){
        etBuy.setText(""+ csBuy);
    }
    @OnClick(R.id.tvZebPayBuy)
    public void loadBuyZeb(){
        etBuy.setText(""+ zebPayModelBuy);
    }

    @OnClick(R.id.tvCsSell)
    public void loadSell(){
        etSell.setText(""+ csSell);
    }
    @OnClick(R.id.tvZebPaySell)
    public void loadSellZeb(){
        etSell.setText(""+zebPayModelSell);
    }

    @OnClick(R.id.tvNewBtc)
    public void loadBTC(){
        etBtc.setText(tvNewBtc.getText().toString());
    }

    private void loadFromSpf() {
        try {
            etBtc.setText(sharedPrefs.getStringPref("btc"));
            etAmount.setText(sharedPrefs.getStringPref("amount"));
            etSell.setText(sharedPrefs.getStringPref("sell"));
            etBuy.setText(sharedPrefs.getStringPref("buy"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final String TAG = MainActivity.class.getSimpleName();
    private void setReceiver() {

        // register a receiver for callbacks
        IntentFilter filter = new IntentFilter("progress_callback");
        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle b = intent.getExtras();
                if(b != null && b.containsKey("exchangeType")) {
                    ExchangeTypes exchangeType = (ExchangeTypes) b.getSerializable("exchangeType");
                    if(exchangeType == ExchangeTypes.COINSECURE) {
                        CoinsecureModel coinsecureModel = (CoinsecureModel) b.getSerializable("coinsecureModel");
                        Log.d(TAG, "onReceive: " + coinsecureModel);
                        if (coinsecureModel != null) {
                            if ("Lowest Ask".equalsIgnoreCase(coinsecureModel.getMethod())) {
                                csBuy = coinsecureModel.getMessage().getRate() / 100;
                                tvCsBuy.setText("CS Buy: \u20B9 " + csBuy);
                            } else if ("Highest Bid".equalsIgnoreCase(coinsecureModel.getMethod())) {
                                csSell = coinsecureModel.getMessage().getRate() / 100;
                                tvCsSell.setText("CS Sell: \u20B9 " + csSell);
                            }
                        }
                    } else if(exchangeType == ExchangeTypes.CRYPTOCOMPARE){
                        CryptoCompareModel cryptoCompareModel = (CryptoCompareModel) b.getSerializable("cryptoCompareModel");
                        Log.d(TAG, "onReceive: " + cryptoCompareModel);
                            tvCryptoUSD.setText("Crypto : $ " + cryptoCompareModel.getUSD());
                            tvCryptoINR.setText("Crypto : \u20B9 " + cryptoCompareModel.getINR());
                    } else if(exchangeType == ExchangeTypes.ZEBPAY){
                        ZebPayModel zebPayModel = (ZebPayModel) b.getSerializable("zebPayModel");
                        Log.d(TAG, "onReceive: " + zebPayModel);
                        zebPayModelBuy = zebPayModel.getBuy();
                        tvZebPayBuy.setText("ZP Buy : \u20B9 " + zebPayModelBuy);
                        zebPayModelSell = zebPayModel.getSell();
                        tvZebPaySell.setText("ZP Sell : \u20B9 " + zebPayModelSell);
                    }
                }
            }
        };
        registerReceiver(receiver, filter);
    }

    @BindView(R.id.seekTimer)
    SeekBar seekBar;

    @BindView(R.id.tvSeekTimer)
    TextView tvSeekTimer;
    public void setSeekbar(){
        final int step = 1;
        int max = 60;
        final int min = 5;

// Ex :
// If you want values from 3 to 5 with a step of 0.1 (3, 3.1, 3.2, ..., 5)
// this means that you have 21 possible values in the seekbar.
// So the range of the seek bar will be [0 ; (5-3)/0.1 = 20].
        seekBar.setMax( (max - min) / step );
        seekBar.setProgress(sharedPrefs.getCallTimer());
        tvSeekTimer.setText(sharedPrefs.getCallTimer() + "s");
        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener()
                {
                    @Override public void onStopTrackingTouch(SeekBar seekBar) {}
                    @Override public void onStartTrackingTouch(SeekBar seekBar) {}

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(fromUser) {
                            int value = min + (progress * step);
                            tvSeekTimer.setText(value + "s");
                            sharedPrefs.saveIntPreference("callTimer", value);
                        }
                    }
                }
        );
    }


    private void saveSpf(double newBtc, double newAmount) {
        try {
            sharedPrefs.saveStringPreference("btc", String.valueOf(newBtc));
            sharedPrefs.saveStringPreference("amount", String.valueOf(newAmount));
            sharedPrefs.saveStringPreference("sell", etSell.getText().toString());
            sharedPrefs.saveStringPreference("buy", etBuy.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.btnCalculate)
    public void calculate() {
        try {
            double btc = Precision.round(Double.parseDouble(etBtc.getText().toString()), 6);
//        double amount = Double.parseDouble(etAmount.getText().toString());
            double sell = Precision.round(Double.parseDouble(etSell.getText().toString()), 6);
            double buy = Precision.round(Double.parseDouble(etBuy.getText().toString()), 6);

            double x = Precision.round(btc * sell, 6);
            double xChange = Precision.round(.004 * x, 6);
            xAmount = Precision.round(x - xChange, 6);

            double y = Precision.round(xAmount / buy, 6);
            double yChange = Precision.round(.004 * y, 6);
            xBTC = Precision.round(y - yChange, 6);

            tvNewBtc.setText("" + BigDecimal.valueOf(xBTC).toPlainString());
            tvProfitLoss.setText("BTC Gained: " + BigDecimal.valueOf(Precision.round((xBTC - btc), 6)).toPlainString());
            double roundProfitLoss = Precision.round(((xBTC - btc) * sell), 6);
            if(roundProfitLoss > 0){
                tvProfitLossRs.setText("Profit INR : " + roundProfitLoss);
                tvProfitLossRs.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            } else {
                tvProfitLossRs.setText("Lost INR : " + (-1 * roundProfitLoss));
                tvProfitLossRs.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.tvSettings)
    public void settingsTextClick(){
        if(llSettings.getVisibility() == View.VISIBLE){
            llSettings.setVisibility(View.GONE);
        } else {
            llSettings.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.btnSave)
    public void save() {
        saveSpf(xBTC, xAmount);
    }

    @OnClick(R.id.btnSellAll)
    public void sellAll() {
        try {
            double sell = Precision.round(Double.parseDouble(etSell.getText().toString()), 6);
            double btc = Precision.round(Double.parseDouble(etBtc.getText().toString()), 6);

            tvNewBtc.setText("INR you should've got: " + Precision.round((btc * sell), 6));
            tvProfitLossRs.setText("Sell all for : " + Precision.round((btc * sell) - .004 * (btc * sell), 6));
            tvProfitLoss.setText("0.4% charges INR: " + Precision.round( .004 * (btc * sell), 6));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.btnBuyAll)
    public void buyAll() {
        try {
            double amount = Double.parseDouble(etAmount.getText().toString());
            double buy = Precision.round(Double.parseDouble(etBuy.getText().toString()), 6);

            tvNewBtc.setText("BTC you should've got: " + BigDecimal.valueOf(Precision.round((amount / buy), 6)).toPlainString());
            tvProfitLossRs.setText("BTC got : " + BigDecimal.valueOf(Precision.round((amount / buy) - .004 * (amount / buy), 6)).toPlainString());
            tvProfitLoss.setText("0.4% charges BTC: " + BigDecimal.valueOf(Precision.round( .004 * (amount / buy), 6)).toPlainString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.ivBuyMinus)
    public void buyMinus(){
        double aDouble = 0;
        try {
            aDouble = Double.parseDouble(etBuy.getText().toString());
        } catch (Exception e){ }
        etBuy.setText("" + (aDouble - 1000));
    }

    @OnClick(R.id.ivBuyPlus)
    public void buyPlus(){
        double aDouble = 0;
        try {
            aDouble = Double.parseDouble(etBuy.getText().toString());
        } catch (Exception e){ }
        etBuy.setText("" + (aDouble + 1000));
    }

    @OnClick(R.id.ivSellMinus)
    public void sellMinus(){
        double aDouble = 0;
        try {
            aDouble = Double.parseDouble(etSell.getText().toString());
        } catch (Exception e){ }
        etSell.setText("" + (aDouble - 1000));
    }

    @OnClick(R.id.ivSellPlus)
    public void sellPlus(){
        double aDouble = 0;
        try {
            aDouble = Double.parseDouble(etSell.getText().toString());
        } catch (Exception e){ }
        etSell.setText("" + (aDouble + 1000));
    }

}
