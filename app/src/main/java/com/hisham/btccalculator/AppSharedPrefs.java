package com.hisham.btccalculator;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

/**
 * Created by Hisham on 28/04/16.
 */
public class AppSharedPrefs {

    private static AppSharedPrefs ourInstance;

    private SharedPreferences preferences;

    private AppSharedPrefs(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static AppSharedPrefs getInstance(Context context) {
        if(ourInstance == null)
            ourInstance = new AppSharedPrefs(context);
        return ourInstance;
    }


    public void saveStringPreference(String key, String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public void saveIntPreference(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();

    }

    public void saveBooleanPreference(String key, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();

    }

    public String getINRDiff() {
        return preferences.getString("CsInrDiff", "5000");
    }
    public String getDollarDiff() {
        return preferences.getString("CryptoDiff", "100");
    }
    public String getStringPref(String key){
        return preferences.getString(key, "0");
    }

    public int getCallTimer(){
        return preferences.getInt("callTimer", 15);
    }

    public boolean getBooleanPref(String key){
        return preferences.getBoolean(key, false);
    }
}
